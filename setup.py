from setuptools import setup

with open("requirements.txt", "r") as outfile:
    requirement = outfile.read()

setup(
    name="facesLIB",
    author="Muhammad Fahmy",
    description="Face Validation Module",
    version=1.0,
    packages=["deepface"],
    author_email="fahmy@varx.com",
    url="https://gitlab.com/VARX/testing_modul",
    keywords=["pip", "pypi"],
    python_requires=">=3.9",
    install_requires=requirement,
)
